package com.iosu.keradgamestest.utils;

import android.app.Application;

/**
 * Created by iosu on 1/2/16.
 */
public class MyApplication extends Application {

    private static MyApplication instance;

    public static MyApplication getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();
    }
}
