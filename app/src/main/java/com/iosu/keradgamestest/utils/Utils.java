package com.iosu.keradgamestest.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by iosu on 1/2/16.
 */
public class Utils {

    public static SimpleDateFormat sdfHour = new SimpleDateFormat("HH", Locale.ENGLISH);
    public static SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
    public static SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm - dd MMM yyyy", Locale.ENGLISH);

    public static boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo  info = cm.getActiveNetworkInfo();
        return (info != null && info.isConnected());
    }

}
