package com.iosu.keradgamestest.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.iosu.keradgamestest.R;
import com.iosu.keradgamestest.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends Delegate implements View.OnClickListener {

    private static final long BACK_DELAY = 1500;
    private static final long RETWEET_DELAY = 10000;
    private boolean isBackPressed = false;
    private ImageView photo;
    private TextView tweet, date, retweet;
    private int retweetNumber = 0;
    private int easterEggCount = 0;

    private boolean started = false;
    private Handler handlerTwitter = new Handler();
    private Runnable runnableTwitter = new Runnable() {
        @Override
        public void run() {
            ramdomizeRetweet();
            if(started) {
                startRetweetTimer();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setViews();
        addContent();
        startRetweetTimer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startRetweetTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopRetweetTimer();
    }

    private void setViews() {
        CardView twitterCard = (CardView) findViewById(R.id.activity_main_card_twitter);
        twitterCard.setOnClickListener(this);
        photo = (ImageView) twitterCard.findViewById(R.id.card_twitter_photo);
        tweet = (TextView) twitterCard.findViewById(R.id.card_twitter_tweet);
        date = (TextView) twitterCard.findViewById(R.id.card_twitter_date);
        retweet = (TextView) twitterCard.findViewById(R.id.card_twitter_retweet);

        FrameLayout easterEgg = (FrameLayout) findViewById(R.id.easter_egg);
        easterEgg.setOnClickListener(this);
    }

    private void addContent() {
        Picasso.with(this).load(getResources().getString(R.string.image_url)).into(photo);
        Date dt = new Date();
        String currentTime = Utils.sdfTime.format(dt);
        int currentHour = Integer.valueOf(Utils.sdfHour.format(dt));
        date.setText(Utils.sdfDate.format(dt));
        tweet.setText(getResources().getString(R.string.twitter_tweet_text_start).concat(" ".concat(currentTime)));
        if (currentHour >= 8 && currentHour < 18) {
            // working
            tweet.setText(tweet.getText().toString().concat(" ").concat(getResources().getString(R.string.twitter_tweet_text_working)));
        } else if (currentHour >= 18 && currentHour < 23) {
            // spare time
            tweet.setText(tweet.getText().toString().concat(" ").concat(getResources().getString(R.string.twitter_tweet_text_spare_time)));
        } else {
            // sleeping
            tweet.setText(tweet.getText().toString().concat(getResources().getString(R.string.twitter_tweet_text_sleeping)));
        }
    }

    /**
     * Retweet ramdomizer
     */
    private void startRetweetTimer() {
        started = true;
        handlerTwitter.postDelayed(runnableTwitter, RETWEET_DELAY);
    }

    public void stopRetweetTimer() {
        started = false;
        handlerTwitter.removeCallbacks(runnableTwitter);
    }

    private void ramdomizeRetweet() {
        // random number between retweetNumber & retweetNumber+5
        final Random random = new Random();
        int retweetMaxNumber = 5 + retweetNumber;
        retweetNumber = random.nextInt(retweetMaxNumber - retweetNumber) + retweetNumber;
        retweet.setText(String.valueOf(retweetNumber));
    }

    /**
     * timer to put isBackPressed boolean false
     */
    private void startTimer() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                isBackPressed = false;
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, BACK_DELAY);
    }

    @Override
    public void onBackPressed() {
        // Double tap to exit
        Toast.makeText(this, getResources().getString(R.string.back_double_tap), Toast.LENGTH_SHORT).show();
        startTimer();
        if (isBackPressed) {
            super.onBackPressed();
            finish();
        }
        isBackPressed = true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_main_card_twitter:
                if (Utils.isOnline()) {
                    // open twitter
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.twitter)));
                    startActivity(intent);
                } else {
                    //show connection error dialog
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.dialog_no_internet_title)
                            .setMessage(R.string.dialog_no_internet_message)
                            .setCancelable(false)
                            .setPositiveButton(R.string.dialog_no_internet_ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            }).create().show();
                }
                break;
            case R.id.easter_egg:
                if (easterEggCount >= 10) {
                    if (Utils.isOnline()) {
                        // open easter egg
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.easter_egg)));
                        startActivity(intent);
                    }

                }
                easterEggCount++;
                break;
        }
    }
}
