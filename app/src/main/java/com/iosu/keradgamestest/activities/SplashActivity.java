package com.iosu.keradgamestest.activities;

import android.content.Intent;
import android.os.Bundle;

import com.iosu.keradgamestest.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by iosu on 31/1/16.
 */
public class SplashActivity extends Delegate {

    // Set the duration of the splash screen
    private static final long mSPLASH_SCREEN_DELAY = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                // Start the next activity
                Intent mainIntent = new Intent().setClass(
                        SplashActivity.this, MainActivity.class);
                startActivity(mainIntent);

                // Close the activity so the user won't able to go back this
                // activity pressing Back button
                finish();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, mSPLASH_SCREEN_DELAY);
    }
}
